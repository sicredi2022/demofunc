public class Funcionario {
    private int matricula;
    private String nome;
    private double salarioBase;
    private int nroDependentes;
    private boolean insalubridade;

    public Funcionario(int mat, String n, double base, int nroDep, boolean ins){
        matricula = mat;
        nome = n;
        salarioBase = base;
        nroDependentes = nroDep;
        insalubridade = ins;
    }

    public int getMatricula() {
        return matricula;
    }

    public String getNome() {
        return nome;
    }

    public double getSalarioBase() {
        return salarioBase;
    }

    public int getNroDependentes() {
        return nroDependentes;
    }

    public boolean getInsalubridade() {
        return insalubridade;
    }

    public void aumentaSalBase(double taxa) {
        salarioBase *= 1 + taxa;
    }

    public double inss() {
        if (getInsalubridade())
            return salarioBase * 0.04;
        else
            return salarioBase * 0.08;
    }

    public double irpf() {
        if (getInsalubridade())
            return salarioBase * 0.10;
        else
            return salarioBase * 0.20;
    }

    public double getSalarioBruto() {
        return getSalarioBase();
    }

    public double getSalarioLiquido() {
        return getSalarioBase() - inss() - irpf();
    }

    public String toString() {
        return ("Nome:" + nome +  ", salarioBase: " + salarioBase);
    }
}
