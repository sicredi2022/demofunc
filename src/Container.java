import java.util.List;

public class Container {
    public <T> Double somatorio(List<T> lst, Condicao<T> condicao, Operacao<T> oper) {
        Double somatorio = 0.0;
        for (T obj : lst) {
            if (condicao.verifica(obj)) {
                somatorio += oper.calcula(obj);
            }
        }
        return somatorio;
    }
}
