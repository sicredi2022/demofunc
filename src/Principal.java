import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class Principal {
    public static void main(String[] args) {
        Container cnt = new Container();
        List<Funcionario> lstf = new ArrayList<>();
        lstf.add(new Funcionario(001, "João", 1000.00, 2, false));
        lstf.add(new Funcionario(002, "Maria", 2500.00, 1, true));
        lstf.add(new Funcionario(003, "Pedro", 5000.00, 0, true));
        lstf.add(new Funcionario(004, "Ana", 3000.00, 3, true));
        lstf.add(new Funcionario(005, "José", 4500.00, 2, false));

        // Condicao<Funcionario> condicao = f -> f.getInsalubridade();
        // Operacao<Funcionario> impostos = f -> f.inss() + f.irpf();
        // Double gastosComImpostos = cnt.somatorio(lstf, condicao, impostos);
        // System.out.println("Impostos dos insalubres: " + gastosComImpostos);

        // condicao = f -> f.getNroDependentes() > 2;
        // impostos = f -> f.getSalarioLiquido();
        // Double gastosComSalarios = cnt.somatorio(lstf, condicao, impostos);
        // System.out.println("Gastos com salarios: " + gastosComSalarios);

        // // Imprime os dados de todos os funcionários
        // lstf.forEach(f -> System.out.println(f));

        // // Exemplos de uso de ForEach
        // // Aumenta o salário base dos funcionários em 10%
        // lstf.forEach(f -> f.aumentaSalBase(0.1));

        // // Imprime os dados de todos os funcionários
        // lstf.forEach(f -> System.out.println(f));

        // // A função andThen
        // Consumer<Funcionario> aumentaSalario = f -> f.aumentaSalBase(0.1);
        // Consumer<Funcionario> imprimeFuncionario = f -> System.out.println(f);
        // lstf.forEach(aumentaSalario.andThen(imprimeFuncionario));

        Double doublesalarioMedio = lstf.stream() // Stream<Funcionario>
                .filter(f -> f.getInsalubridade()) // Stream<Funcionario>
                .mapToDouble(f -> f.getSalarioLiquido()) // Stream<Double>
                .average() // Optional<Double>
                .getAsDouble();

        System.out.println("Média dos salarios liquidos: " + doublesalarioMedio);

        List<String> nomes = lstf.stream() // Stream<Funcionario>
                .filter(f -> f.getNroDependentes() > 0) // Stream<Funcionario>
                .filter(f -> f.getInsalubridade())
                .map(f -> f.getNome()) // Stream<String>
                .collect(Collectors.toList());

        nomes.forEach(s -> System.out.println(s + " "));
    }
}
